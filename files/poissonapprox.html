<body class="statsnotes">
  <div class="content">
    <section id="introduction">
      <h1 id="header_content">When can Poisson Regression approximate Logistic?</h1>
      <p>
        An older idea in Epidemiology is to use a Poisson regression model in
        place of a logistic regression model. This idea has some validity
        because with a low mean, the Poisson distribution approximates the
        binary distribution.
      </p>
    </section>
    <section id="simulation">
      <h2>Simulation</h2>

      <p>
        Let’s examine this. First, we define a program which generates some data
        according to a logistic model, then fits both logistic and Poisson
        regression models against it.
      </p>

      <p>
        This program, defined below, takes in three arguments:
      </p>

      <ul>
        <li><code>n</code> - Sample size</li>
        <li><code>p</code> - Baseline probability of success</li>
        <li><code>b1</code> - Coefficient of interest.</li>
      </ul>

      <p>
        The model is simply
      </p>

      <math display="block" class="tml-display" style="display:block math;">
        <mrow>
          <mtext>logit</mtext>
          <mo form="prefix" stretchy="false">(</mo>
          <mi>P</mi>
          <mo form="prefix" stretchy="false">(</mo>
          <mi>Y</mi>
          <mo>=</mo>
          <mn>1</mn>
          <mi>|</mi>
          <mi>X</mi>
          <mo form="postfix" stretchy="false">)</mo>
          <mo form="postfix" stretchy="false">)</mo>
          <mo>=</mo>
          <mtext>logit</mtext>
          <mo form="prefix" stretchy="false">(</mo>
          <mi>p</mi>
          <mo form="postfix" stretchy="false">)</mo>
          <mo>+</mo>
          <msub>
            <mi>b</mi>
            <mn>1</mn>
          </msub>
          <mi>x</mi>
        </mrow>
      </math>

      <pre>program def binsim, rclass
    drop _all
    args n p b1
    set obs `n'
    gen x = rnormal()
    gen y = rbinomial(1, invlogit(logit(`p') + `b1'*x))
    * Return P(success) to ensure everything is working
    mean y
    mat b = e(b)
    scalar pp = b[1,1]
    return scalar pp=pp

    * Poisson model
    poisson y x
    mat b = e(b)
    scalar b_pois = b[1,1]
    return scalar b_pois=b_pois

    * Logistic model
    logistic y x
    mat b = e(b)
    scalar b_logit = b[1,1]
    return scalar b_logit=b_logit
end</pre>

    </section>
    <section id="results">
      <h2 style={'margin:0px'}>Results</h2>
      <div class="poisson-select">

        <div class="poisson-tabs">
          Prevalence:
          <label>
            <input
              type="radio"
              name="poisson-prev"
              id="prev10"
              checked
            >
            10%
          </label>
          <label>
            <input
              type="radio"
              name="poisson-prev"
              id="prev5"
            >
            5%
          </label>
          <label>
            <input
              type="radio"
              name="poisson-prev"
              id="prev3"
            >
            3%
          </label>
          <label>
            <input
              type="radio"
              name="poisson-prev"
              id="prev1"
            >
            1%
          </label>
          <label>
            <input
              type="radio"
              name="poisson-prev"
              id="prevp5"
            >
            .5%
          </label>
        </div>


        <p>
          We'll let <code>b1</code> = .4 with <code>n</code> = 10,000.
          (<code>n</code> needs to be large enough such that we have a
          nontrivial amount of successes.) Choose from the tabs above the
          <code>p</code> to see how the results vary as the prevalence
          approaches 0.
        </p>

        <p>
          Run the simulation. We save the estimated proportion of successes
          (<code>pp</code>)to ensure the simulation worked as intended, as
          well as the estimated coefficients from the Poisson model
          (<code>b_pois</code>) and logistic model (<code>b_logit</code>).
        </p>

        <pre>. simulate pp=r(pp) b_pois=r(b_pois) b_logit=r(b_logit), ///
>     reps(1000) nodots: binsim 10000 <span class="sim_prop"><span>.1</span><span>.05</span><span>.03</span><span>.01</span><span>.005</span></span> .4

      Command: binsim 10000 <span class="sim_prop"><span>.1</span><span>.05</span><span>.03</span><span>.01</span><span>.005</span></span> .4
           pp: r(pp)
       b_pois: r(b_pois)
      b_logit: r(b_logit)</pre>

        <p>
          First we’ll ensure the code is working and that proportion of
          positive outcomes is approximately
          <span class="sim_prop">
            <span>.1.</span> <!-- trailing . for spacing -->
            <span>.05.</span>
            <span>.03.</span>
            <span>.01.</span>
            <span>.005.</span>
          </span>
        </p>

        <div class="sim_results">
          <pre>. mean pp

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
          pp |   .1057365   .0000978      .1055445    .1059285
--------------------------------------------------------------</pre>
          <pre>. mean pp

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
          pp |   .0535497   .0000704      .0534115    .0536879
--------------------------------------------------------------</pre>
          <pre>. mean pp

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
          pp |   .0322263   .0000573      .0321139    .0323387
--------------------------------------------------------------</pre>
          <pre>. mean pp

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
          pp |   .0108073   .0000342      .0107402    .0108744
--------------------------------------------------------------</pre>
          <pre>. mean pp

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
          pp |   .0053893   .0000223      .0053454    .0054332
--------------------------------------------------------------</pre>
        </div>

        <p>
          Now we can examine the distributions of the two estimated
          coefficients. If Poisson is truly a good approximation, then the two
          distributions should be nearly identical.
        </p>

        <pre>. twoway kdensity b_logit || kdensity b_pois, ///
>     xline(.4) legend(label(1 "Logistic") label(2 "Poisson"))</pre>

        <div class="poisson_images">
          <img src="images/poisson10.svg" class="image_center image_invert">
          <img src="images/poisson5.svg" class="image_center image_invert">
          <img src="images/poisson3.svg" class="image_center image_invert">
          <img src="images/poisson1.svg" class="image_center image_invert">
          <img src="images/poisson.5.svg" class="image_center image_invert">
        </div>

        <p>
          We can estimate the proportion of bias.
        </p>

        <div class="sim_results">
          <pre>. gen error = abs(b_logit - b_pois)/b_logit

. mean error

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
       error |   .1195278   .0001314      .1192699    .1197857
--------------------------------------------------------------</pre>
          <pre>. gen error = abs(b_logit - b_pois)/b_logit

. mean error

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
       error |   .0616367   .0001023      .0614361    .0618374
--------------------------------------------------------------</pre>
          <pre>. gen error = abs(b_logit - b_pois)/b_logit

. mean error

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
       error |   .0375324   .0000839      .0373677    .0376971
--------------------------------------------------------------</pre>
          <pre>. gen error = abs(b_logit - b_pois)/b_logit

. mean error

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
       error |   .0127062   .0000505      .0126072    .0128053
--------------------------------------------------------------</pre>
          <pre>. gen error = abs(b_logit - b_pois)/b_logit

. mean error

Mean estimation                          Number of obs = 1,000

--------------------------------------------------------------
             |       Mean   Std. err.     [95% conf. interval]
-------------+------------------------------------------------
       error |   .0064606   .0000391      .0063839    .0065374
--------------------------------------------------------------</pre>

        </div>
      </div>
    </section>

    <section id="conclusion">
      <h2>Conclusion</h2>
      <p>
        When the prevalence is very low, Poisson is not a bad approximation, but
        I wouldn’t recommend using it over Logistic unless prevalence was 1% or
        less.
      </p>
    </section>
    <img src="https://errickson.goatcounter.com/count?p=poissonapprox">

  </div>
  <nav class="section-nav">
    <h2 id="header_nav">When can Poisson Regression approximate Logistic?</h2>
	  <ol>
		  <li class=""><a href="#introduction">Introduction</a></li>
		  <li class=""><a href="#simulation">Simulation</a></li>
		  <li class=""><a href="#results">Results</a></li>
		  <li class=""><a href="#conclusion">Conclusion</a></li>
    </ol>
