<body class="statsnotes">
  <link rel="stylesheet" href="viscol.css">
  <div class="content">
    <section id="introduction">
      <h1 id="header_content">Visualizing Collinearity</h1>
      <p>
        While there are many resources out there to describe the issues arising
        with multicollinearity in independent variables, there’s a visualization
        for the problem that I came across in a book once and haven’t found
        replicated online. This replicates the visualization.
      </p>
    </section>
    <section id="set-up">
      <h2>Set-up</h2>
      <p>
        Let Y be a response and X<sub>1</sub> and X<sub>2</sub> be the predictors, such
        that
      </p>

      <math display="block" class="tml-display" style="display:block math;">
        <mrow>
          <msub>
            <mi>Y</mi>
            <mi>i</mi>
          </msub>
          <mo>=</mo>
          <msub>
            <mi>&beta;</mi>
            <mn>0</mn>
          </msub>
          <mo>+</mo>
          <msub>
            <mi>&beta;</mi>
            <mn>1</mn>
          </msub>
          <msub>
            <mi>X</mi>
            <mrow>
              <mn>1</mn>
              <mi>i</mi>
            </mrow>
          </msub>
          <mo>+</mo>
          <msub>
            <mi>&beta;</mi>
            <mn>2</mn>
          </msub>
          <msub>
            <mi>X</mi>
            <mrow>
              <mn>2</mn>
              <mi>i</mi>
            </mrow>
          </msub>
          <mo>+</mo>
          <msub>
            <mi>&epsilon;</mi>
            <mi>i</mi>
          </msub>
        </mrow>
      </math>

      <p>
        for individual <i>i</i>.
      </p>

      <p>
        For simplicity, let’s say that
      </p>

      <math display="block" class="tml-display" style="display:block math;">
        <mrow>
          <msub>
            <mi>&beta;</mi>
            <mn>0</mn>
          </msub>
          <mo>=</mo>
          <mn>0</mn>
          <mo separator="true">,</mo>
          <mo></mo>
          <msub>
            <mi>&beta;</mi>
            <mn>1</mn>
          </msub>
          <mo>=</mo>
          <mn>1</mn>
          <mo separator="true">,</mo>
          <mo></mo>
          <msub>
            <mi>&beta;</mi>
            <mn>2</mn>
          </msub>
          <mo>=</mo>
          <mn>1</mn>
          <mi>.</mi>
        </mrow>
      </math>
      <p>
        I carry out a simulation by generating 1,000 data-sets with a specific
        correlation between predictors and obtain their coefficients.
      </p>

      <pre>reps <span class="operator"><-</span> <span class="numeric">1000</span>
        n <span class="operator"><-</span> <span class="numeric">100</span>
        save <span class="operator"><-</span> <span class="keyword">matrix</span>(<span class="keyword">nrow</span> <span class="operator">=</span> reps, <span class="keyword">ncol</span> <span class="operator">=</span> <span class="numeric">3</span>)

        <span class="keyword">for</span> (i <span class="keyword">in</span> <span class="numeric">1</span><span class="operator">:</span>reps) {
        x1 <span class="operator"><-</span> <span class="keyword">rnorm</span>(n)
          x2 <span class="operator"><-</span> <span class="keyword">rnorm</span>(n)
          y <span class="operator"><-</span> x1 <span class="operator">+</span> x2 <span class="operator">+</span> <span class="keyword">rnorm</span>(n)

          mod <span class="operator"><-</span> <span class="keyword">lm</span>(y <span class="operator">~</span> x1 <span class="operator">+</span> x2)
          save[i, ] <span class="operator"><-</span> <span class="keyword">c</span>(<span class="keyword">coef</span>(mod)[<span class="numeric">-1</span>], <span class="keyword">cor</span>(x1, x2))
          }</pre>

          <p>
            The line <code>x2 &lt;- rnorm(n)</code> gets replaced with <code>x2
        &lt;- x1 + rnorm(n, sd = _)</code>, where the <code>_</code> is replaced
            with difference values to induce more correlation
            between <code>x1</code> and <code>x2</code>.
          </p>
    </section>
    <section id="simulation">
      <h2>Simulation Results</h2>


      <div class="col-select">
        <label>
          <input
            type="radio"
            name="collinearity_strength"
            value="low"
            id="low"
            checked
          >
          Low
        </label>

        <label>
          <input
            type="radio"
            name="collinearity_strength"
            value="mod"
            id="mod"
          >
          Moderate
        </label>

        <label>
          <input
            type="radio"
            name="collinearity_strength"
            value="high"
            id="high"
          >
          High
        </label>

        <label>
          <input
            type="radio"
            name="collinearity_strength"
            value="extreme"
            id="extreme"
          >
          Extreme
        </label>

        <div class="col-plot-container">
          <div class="col-images">
            <img src="images/corrplot_low.png" class="image_invert">
            <img src="images/corrplot_mod.png" class="image_invert">
            <img src="images/corrplot_high.png" class="image_invert">
            <img src="images/corrplot_extreme.png" class="image_invert">
          </div>
          <div class="collinearity_graph_text">
            <p>
              Each point represents the estimated coefficients for a single
              simulated data set. The red dot represents the data-generating
              coefficients (1, 1). Note that bias is not a concern; the true
              coefficients are on average for each level of collinearity.
            </p>

            <p>
              In this simulation, the average correlation between X<sub>1</sub> and
              X<sub>2</sub> is
              <span class="col-corr">
                <span>0.002.</span> <!-- The trailing . is so there's no extra space -->
                <span>0.554.</span>
                <span>0.894.</span>
                <span>0.995.</span>
              </span>
            </p>

            <div class="col-text">
              <span>
                With low correlation, we see no relationship between the estimated
                coefficients.
              </span>
              <span>
                While we are starting to see a correlation between the
                coefficients, it is not that strong yet.
              </span>
              <span>
                With correlation around .9, we are seeing very strong
                collinearity: if
                <math>
                  <msub>
                    <mover>
                      <mi>X</mi>
                      <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
                    </mover>
                    <mn>1</mn>
                  </msub>
                </math>
                is low, then we would expect
                <math>
                  <msub>
                    <mover>
                      <mi>X</mi>
                      <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
                    </mover>
                    <mn>2</mn>
                  </msub>
                </math>
                to be high.
              </span>
              <span>
                This relationship is incredibly strong.
              </span>
            </div>

          </div>


        </div>
      </div>
    </section>
    <section id="why-is-this-a-problem">
      <h2>Why is this a problem?</h2>
      <p>
        So this simulation shows that at correlations around .9 or higher
        between X<sub>1</sub> and X<sub>2</sub>, there is negative correlation
        between
        <math>
          <msub>
            <mover>
              <mi>X</mi>
              <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
            </mover>
            <mn>1</mn>
          </msub>
        </math>

        and

        <math>
          <msub>
            <mover>
              <mi>X</mi>
              <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
            </mover>
            <mn>2</mn>
          </msub>
        </math>

        . Why is this a problem?
      </p>

      <p>
        Consider the “extremely high correlation” results. With such high
        correlation, we have that
        <math>
          <mrow>
            <msub>
              <mi>X</mi>
              <mn>1</mn>
            </msub>
            <mo>≈</mo>
          </mrow>
          <mrow>
            <msub>
              <mi>X</mi>
              <mn>2</mn>
            </msub>
          </mrow>
        </math>
        We can use this approximate equality to rewrite the model:
      </p>

      <math display="block" class="tml-display" style="display:block math;">
        <mtable displaystyle="true" columnalign="right left">
          <mtr>
            <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
              <msub>
                <mi>Y</mi>
                <mi>i</mi>
              </msub>
            </mtd>
            <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow>
                <mo>=</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>0</mn>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>1</mn>
                </msub>
                <msub>
                  <mi>X</mi>
                  <mrow>
                    <mn>1</mn>
                    <mi>i</mi>
                  </mrow>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>2</mn>
                </msub>
                <msub>
                  <mi>X</mi>
                  <mrow>
                    <mn>2</mn>
                    <mi>i</mi>
                  </mrow>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&epsilon;</mi>
                  <mi>i</mi>
                </msub>
              </mrow>
            </mtd>
          </mtr>
          <mtr>
            <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow></mrow>
            </mtd>
            <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow>
                <mo>≈</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>0</mn>
                </msub>
                <mo>+</mo>
                <mo form="prefix" stretchy="false">(</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>1</mn>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>2</mn>
                </msub>
                <mo form="postfix" stretchy="false">)</mo>
                <msub>
                  <mi>X</mi>
                  <mrow>
                    <mn>1</mn>
                    <mi>i</mi>
                  </mrow>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&epsilon;</mi>
                  <mi>i</mi>
                </msub>
              </mrow>
            </mtd>
          </mtr>
          <mtr>
            <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow></mrow>
            </mtd>
            <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
              <mrow>
                <mo>≈</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>0</mn>
                </msub>
                <mo>+</mo>
                <mo form="prefix" stretchy="false">(</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>1</mn>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&beta;</mi>
                  <mn>2</mn>
                </msub>
                <mo form="postfix" stretchy="false">)</mo>
                <msub>
                  <mi>X</mi>
                  <mrow>
                    <mn>2</mn>
                    <mi>i</mi>
                  </mrow>
                </msub>
                <mo>+</mo>
                <msub>
                  <mi>&epsilon;</mi>
                  <mi>i</mi>
                </msub>
              </mrow>
            </mtd>
          </mtr>
        </mtable>
      </math>

      <p>
        In other words, the model has that
        <math>
          <mrow>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mo>+</mo>
          </mrow>
          <mrow>
            <msub>
              <mi>&beta;</mi>
              <mn>2</mn>
            </msub>
            <mo>=</mo>
          </mrow>
          <mrow>
            <mn>2</mn>
          </mrow>
        </math>
        (since we assumed above that both coefficients have values of 1). So
        while all of those models would have the same predictive power for Y,
        they would have drastically different interpretations. For example, we
        could obtain
        <math>
          <mrow>
            <msub>
              <mover>
                <mi>X</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
              <mn>1</mn>
            </msub>
            <mo>=</mo>
          </mrow>
          <mrow>
            <mo form="prefix" stretchy="false">−</mo>
          </mrow>
          <mrow>
            <mn>1</mn>
          </mrow>
        </math>
        and
        <math>
          <mrow>
            <msub>
              <mover>
                <mi>X</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
              <mn>2</mn>
            </msub>
            <mo>=</mo>
          </mrow>
          <mrow>
            <mn>3</mn>
          </mrow>
        </math>
        , which not only
        over-emphasizes the relationship betwen X<sub>2</sub> and Y, but suggests a inverse
        relationship between X<sub>1</sub> and Y!
      </p>
    </section>
    <img src="https://errickson.goatcounter.com/count?p=visualizeCollinearity">
  </div>
  <nav class="section-nav">
    <h2 id="header_nav">Visualizing Collinearity</h2>
	  <ol>
		  <li class=""><a href="#introduction">Introduction</a></li>
		  <li class=""><a href="#set-up">Set-up</a></li>
		  <li class=""><a href="#simulation">Simulation</a></li>
		  <li class=""><a href="#why-is-this-a-problem">Why is this a Problem?</a></li>
    </ol>
