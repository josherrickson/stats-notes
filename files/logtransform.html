<body class="statsnotes">
  <div class="content">
    <h1 id="header_content">Log Transform Interpretation</h1>

    <section id="introduction">
      <h2>Introduction</h2>
      <p>
        Log transformations can be useful when a variable is very right-skewed,
        or multiplicative effects are desired over additive. However,
        interpretation can be challenging.
      </p>

      <p>
        We are always discussing the natural log (ln), i.e. log<sub>e</sub> .
      </p>

      <section id="introduction_change">
        <h3>Multiplicative vs Percent Change</h3>
        <p>
          Note that multiplicative changes can be expressed as percent changes
          and vice-versa. For example, multiplying a value by 1.3 is equivalent
          to increasing the value by 30&percnt;, or conversely, decreasing a
          value by 15&percnt; is equivalent to multiplying it by .85.
        </p>
      </section>
    </section>
    <section id="logged-outcome">
      <h2>Logged Outcome</h2>
      <p>
        A 1 unit change in a predictor is associated with a
        <math><mrow><mrow><mi>exp</mi><mo>⁡</mo></mrow><mo form="prefix" stretchy="false">(</mo><mover><mi>&beta;</mi><mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo></mover><mo form="postfix" stretchy="false">)</mo></mrow></math> multiplicative
        change in Y, or a <math><mrow><mo form="prefix" stretchy="false">(</mo><mn>100</mn><mo>*</mo><mo form="prefix" stretchy="false">(</mo><mrow><mi>exp</mi><mo>⁡</mo></mrow><mo form="prefix" stretchy="false">(</mo><mover><mi>&beta;</mi><mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo></mover><mo form="postfix" stretchy="false">)</mo><mo>−</mo><mn>1</mn><mo form="postfix" stretchy="false">)</mo><mo form="prefix" stretchy="false">)</mo></mrow></math>&percnt; change in Y.
      </p>

      <p>
        Examples:
      </p>

      <ul>
        <li>
          If
          <math>
            <mover>
              <mi>&beta;</mi>
              <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
            </mover>
          </math> is
          .2, a 1-unit increase in X is associated with an
          <math>
            <mrow>
              <mrow>
                <mi>exp</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mi>.</mi>
              <mn>2</mn>
              <mo form="postfix" stretchy="false">)</mo>
              <mo>≈</mo>
            </mrow>
            <mrow>
              <mn>1.22</mn>
            </mrow>
          </math>
          multiplicative change in Y, or a 22&percnt; increase.
        </li>
        <li>
          If
          <math>
            <mover>
              <mi>&beta;</mi>
              <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
            </mover>
          </math>
          is -.4 a 1-unit increase in X is associated with an
          <math>
            <mrow>
              <mrow>
                <mi>exp</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mo form="prefix" stretchy="false">−</mo>
              <mn>.4</mn>
              <mo form="postfix" stretchy="false">)</mo>
              <mo>≈</mo>
            </mrow>
            <mrow>
              <mi>.</mi>
              <mn>67</mn>
            </mrow>
          </math>
          multiplicative change in Y, or a 33&percnt; <strong>de</strong>crease.
        </li>
      </ul>
      <section id="logged-outcome_theory">
        <h3>Theory</h3>

        <p>
          Assume our regression equation is
        </p>

        <math display="block">
          <mrow>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>Y</mi>
            <mi>|</mi>
            <mi>X</mi>
            <mo>=</mo>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>=</mo>
          </mrow>
          <mrow>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
          </mrow>
          <mrow>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mi>x</mi>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          If we regress on the log of Y instead,
        </p>


        <math display="block">
          <mrow>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>Y</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mi>|</mi>
            <mi>X</mi>
            <mo>=</mo>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>=</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mi>x</mi>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          By <a href="https://en.wikipedia.org/wiki/Taylor_series">Taylor
          expansion</a>,
        </p>

        <math display="block">
          <mrow>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>X</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>≈</mo>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>X</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo form="postfix" stretchy="false">)</mo>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          Therefore we can write
        </p>

        <math display="block" class="tml-display" style="display:block math;">
          <mtable displaystyle="true" columnalign="right left">
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mi>E</mi>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>Y</mi>
                  <mi>|</mi>
                  <mi>X</mi>
                  <mo>=</mo>
                  <mi>x</mi>
                  <mo>+</mo>
                  <mn>1</mn>
                  <mo form="postfix" stretchy="false">)</mo>
                </mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <mtext>exp</mtext>
                  <mrow>
                    <mo fence="true" form="prefix">(</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>0</mn>
                    </msub>
                    <mo>+</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>1</mn>
                    </msub>
                    <mo form="prefix" stretchy="false">(</mo>
                    <mi>x</mi>
                    <mo>+</mo>
                    <mn>1</mn>
                    <mo form="postfix" stretchy="false">)</mo>
                    <mo fence="true" form="postfix">)</mo>
                  </mrow>
                </mrow>
              </mtd>
            </mtr>
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow></mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <mtext>exp</mtext>
                  <mrow>
                    <mo fence="true" form="prefix">(</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>0</mn>
                    </msub>
                    <mo>+</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>1</mn>
                    </msub>
                    <mi>x</mi>
                    <mo>+</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>1</mn>
                    </msub>
                    <mo fence="true" form="postfix">)</mo>
                  </mrow>
                </mrow>
              </mtd>
            </mtr>
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow></mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <mtext>exp</mtext>
                  <mrow>
                    <mo fence="true" form="prefix">(</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>0</mn>
                    </msub>
                    <mo>+</mo>
                    <msub>
                      <mi>&beta;</mi>
                      <mn>1</mn>
                    </msub>
                    <mi>x</mi>
                    <mo fence="true" form="postfix">)</mo>
                  </mrow>
                  <mtext>exp</mtext>
                  <mo form="prefix" stretchy="false">(</mo>
                  <msub>
                    <mi>&beta;</mi>
                    <mn>1</mn>
                  </msub>
                  <mo form="postfix" stretchy="false">)</mo>
                </mrow>
              </mtd>
            </mtr>
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow></mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <mi>E</mi>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>Y</mi>
                  <mi>|</mi>
                  <mi>X</mi>
                  <mo>=</mo>
                  <mi>x</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mtext>exp</mtext>
                  <mo form="prefix" stretchy="false">(</mo>
                  <msub>
                    <mi>&beta;</mi>
                    <mn>1</mn>
                  </msub>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mi>.</mi>
                </mrow>
              </mtd>
            </mtr>
          </mtable>
        </math>

      </section>
      <section id="logged-outcome_example">
        <h3>Example</h3>

        <pre data-lang="R"><code>data(mtcars)
(mod1 <- lm(log(disp) ~ drat, data = mtcars))</code></pre>

          <pre data-lang="R output">Call:
lm(formula = log(disp) ~ drat, data = mtcars)

Coefficients:
(Intercept)         drat
     8.2782      -0.8323</pre>

          <p>
            Therefore a 1-unit increase in <code>drat</code> is associated
            with an
            <math>
              <mrow>
                <mrow>
                  <mi>exp</mi>
                  <mo>⁡</mo>
                </mrow>
                <mo form="prefix" stretchy="false">(</mo>
                <mo form="prefix" stretchy="false">−</mo>
                <mi>.</mi>
                <mn>8323</mn>
                <mo form="postfix" stretchy="false">)</mo>
                <mo>≈</mo>
              </mrow>
              <mrow>
                <mi>.</mi>
                <mn>435</mn>
              </mrow>
            </math>
            multiplicative change in <code>disp</code>, corresponding to a
            56.5&percnt; decrease.
          </p>
          <p>
            To test this, we predict the ratio in predicted outcome with some
            values of <code>drat</code>, and that value increased by
            1. <strong>Note</strong>: We exponentiate the predicted values
            to get them on the outcome scale.
          </p>

          <pre data-lang="R"><code>exp(predict(mod1, newdata = data.frame(drat = 5)))/exp(predict(mod1, newdata = data.frame(drat = 4)))</code></pre>

          <pre data-lang="R output"><code>        1
0.4350567</code></pre>

          <p>
            Repeat with different values of <code>drat</code> to show that all
            that matters is the change in the predictor, not its starting value.
          </p>

          <pre data-lang="R"><code>exp(predict(mod1, newdata = data.frame(drat = 5)))/exp(predict(mod1, newdata = data.frame(drat = 4)))</code></pre>

          <pre data-lang="R output"><code>        1
0.4350567</code></pre>

      </section>
      <section id="logged-outcome_viz">
        <h3>Visualization</h3>
        <p>
          We can visualize this relationship by basic plotting commands. First,
          generate predicted values. We will do this by generating an artificial
          X variable (<code>drat</code>) spaced over its range, obtaining
          predicted values, and then exponentiating them.
        </p>

        <pre data-lang="R"><code>new_drat <- seq(min(mtcarsdrat),
                                             max(mtcarsdrat),
                                             length.out = 100)
                                             yhat1 <- exp(predict(mod1, newdata = data.frame(drat = new_drat)))</code></pre>

          <p>
            Take note of the call to <code>exp()</code> in the line defining
            <code>yhat1</code>.
          </p>

          <p>
            Next, we can plot the best fit line, overlaying on top of the
            observed values.
          </p>

          <pre data-lang="R"><code>plot(yhat1 ~ new_drat, type = "l")
with(mtcars, points(disp ~ drat))</code></pre>

          <img src="images/logy.svg" class="image_invert image_center"
               alt="Plot of disp versus drat from the model where disp (the outcome) is log-transformed">

      </section>
    </section>

    <section id="logged-predictor">
      <h2>Logged Predictor</h2>
      <p>
        A k&percnt; change in a predictor is associated with
        <math>
          <mrow>
            <mover>
              <mi>&beta;</mi>
              <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
            </mover>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mrow>
              <mo fence="true" form="prefix">(</mo>
              <mn>1</mn>
              <mo>+</mo>
              <mfrac>
                <mi>k</mi>
                <mn>100</mn>
              </mfrac>
              <mo fence="true" form="postfix">)</mo>
            </mrow>
          </mrow>
        </math>
        change in the
        outcome.
      </p>

      <p>
        Examples:
      </p>

      <ul>
        <li>
          If <math><mover><mi>&beta;</mi><mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo></mover></math> is 2, a 10&percnt; increase in X is
          associated with a
          <math>
            <mrow>
              <mn>2</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mrow>
                <mo fence="true" form="prefix">(</mo>
                <mn>1</mn>
                <mo>+</mo>
                <mfrac>
                  <mn>10</mn>
                  <mn>100</mn>
                </mfrac>
                <mo fence="true" form="postfix">)</mo>
              </mrow>
              <mo>=</mo>
            </mrow>
            <mrow>
              <mn>2</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mn>1.1</mn>
              <mo form="postfix" stretchy="false">)</mo>
              <mo>≈</mo>
            </mrow>
            <mrow>
              <mn>0.19</mn>
            </mrow>
          </math>
          increase in Y.
        </li>
        <li>
          If <math><mover><mi>&beta;</mi><mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo></mover></math> is -1.51, a 5&percnt; increase in X is
          associated with a
          <math>
            <mrow>
              <mo>−</mo>
            </mrow>
            <mrow>
              <mn>1.5</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mrow>
                <mo fence="true" form="prefix">(</mo>
                <mn>1</mn>
                <mo>+</mo>
                <mfrac>
                  <mn>5</mn>
                  <mn>100</mn>
                </mfrac>
                <mo fence="true" form="postfix">)</mo>
              </mrow>
              <mo>=</mo>
            </mrow>
            <mrow>
              <mo form="prefix" stretchy="false">−</mo>
            </mrow>
            <mrow>
              <mn>1.5</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mn>1.05</mn>
              <mo form="postfix" stretchy="false">)</mo>
              <mo>≈</mo>
            </mrow>
            <mrow>
              <mi>.</mi>
              <mn>03</mn>
            </mrow>
          </math>
          <strong>de</strong>crease in Y.
        </li>
        <li>
          If <math><mover><mi>&beta;</mi><mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo></mover></math> is .75, a 5&percnt; <strong>de</strong>crease in
          X is associated with a
          <math>
            <mrow>
              <mi>.</mi>
              <mn>75</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mrow>
                <mo fence="true" form="prefix">(</mo>
                <mn>1</mn>
                <mo>+</mo>
                <mfrac>
                  <mrow>
                    <mo form="prefix" stretchy="false" lspace="0em" rspace="0em">−</mo>
                    <mn>15</mn>
                  </mrow>
                  <mn>100</mn>
                </mfrac>
                <mo fence="true" form="postfix">)</mo>
              </mrow>
              <mo>=</mo>
            </mrow>
            <mrow>
              <mi>.</mi>
              <mn>75</mn>
              <mrow>
                <mspace width="0.1667em"></mspace>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mi>.</mi>
              <mn>85</mn>
              <mo form="postfix" stretchy="false">)</mo>
              <mo>≈</mo>
            </mrow>
            <mrow>
              <mi>.</mi>
              <mn>05</mn>
            </mrow>
          </math> <strong>de</strong>crease in Y.
        </li>
      </ul>

      <section id="logged-predictor_theory">
        <h3>Theory</h3>

        <p>
          Assume our regression equation is
        </p>


        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>Y</mi>
            <mi>|</mi>
            <mi>X</mi>
            <mo>=</mo>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>=</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mi>x</mi>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          If we include
          <math>
            <mrow>
              <mrow>
                <mi>log</mi>
                <mo>⁡</mo>
              </mrow>
              <mo form="prefix" stretchy="false">(</mo>
              <mi>x</mi>
              <mo form="postfix" stretchy="false">)</mo>
            </mrow>
          </math>
          instead, we have
        </p>


        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>Y</mi>
            <mi>|</mi>
            <mi>X</mi>
            <mo>=</mo>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>=</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          Consider when
          <math>
            <mrow>
              <mi>X</mi>
              <mo>=</mo>
            </mrow>
            <mrow>
              <mi>c</mi>
              <mi>x</mi>
            </mrow>
          </math>
          where
          <math>
            <mi>c</mi>
          </math>
          is some constant (e.g. 2 for a
          doubling of X or 1.3 for a 30&percnt; increase in X).
        </p>


        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>E</mi>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>Y</mi>
            <mi>|</mi>
            <mi>X</mi>
            <mo>=</mo>
            <mi>c</mi>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mo>=</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mrow>
              <mi>log</mi>
              <mo>⁡</mo>
            </mrow>
            <mo form="prefix" stretchy="false">(</mo>
            <mi>c</mi>
            <mi>x</mi>
            <mo form="postfix" stretchy="false">)</mo>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          Therefore if we look at the difference in expectation,
        </p>
        <math display="block" class="tml-display" style="display:block math;">
          <mtable displaystyle="true" columnalign="right left">
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mi>E</mi>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>Y</mi>
                  <mi>|</mi>
                  <mi>X</mi>
                  <mo>=</mo>
                  <mi>c</mi>
                  <mi>x</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mo>−</mo>
                  <mi>E</mi>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>Y</mi>
                  <mi>|</mi>
                  <mi>X</mi>
                  <mo>=</mo>
                  <mi>x</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                </mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <msub>
                    <mi>&beta;</mi>
                    <mn>1</mn>
                  </msub>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mrow>
                    <mi>log</mi>
                    <mo>⁡</mo>
                  </mrow>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>c</mi>
                  <mi>x</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mo>−</mo>
                  <mrow>
                    <mi>log</mi>
                    <mo>⁡</mo>
                  </mrow>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>x</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mo form="postfix" stretchy="false">)</mo>
                </mrow>
              </mtd>
            </mtr>
            <mtr>
              <mtd class="tml-right" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow></mrow>
              </mtd>
              <mtd class="tml-left" style="padding:0.7ex 0em 0.7ex 0em;">
                <mrow>
                  <mo>=</mo>
                  <msub>
                    <mi>&beta;</mi>
                    <mn>1</mn>
                  </msub>
                  <mrow>
                    <mi>log</mi>
                    <mo>⁡</mo>
                  </mrow>
                  <mo form="prefix" stretchy="false">(</mo>
                  <mi>c</mi>
                  <mo form="postfix" stretchy="false">)</mo>
                  <mi>.</mi>
                </mrow>
              </mtd>
            </mtr>
          </mtable>
        </math>

        <section id="logged-predictor_approximation">
          <h4>Approximation</h4>

          <p>
            If your percent change is small (a few percent) then you can
            approximate the change. This is because
            <math>
              <mrow>
                <mrow>
                  <mi>log</mi>
                  <mo>⁡</mo>
                </mrow>
                <mo form="prefix" stretchy="false">(</mo>
                <mn>1</mn>
                <mo>+</mo>
                <mi>x</mi>
                <mo form="postfix" stretchy="false">)</mo>
                <mo>≈</mo>
              </mrow>
              <mrow>
                <mi>x</mi>
              </mrow>
            </math>
            when
            <math>
              <mi>x</mi>
            </math>
            is close to 0. So to approximate what effect a 1&percnt; change in X
            would have, simply multiply
            <math>
              <mover>
                <mi>&beta;</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
            </math>
            by that value:
            <math>
              <mrow>
                <mn>0.1</mn>
                <mover>
                  <mi>&beta;</mi>
                  <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
                </mover>
              </mrow>
            </math>
            . This works reliably well up to
            <math>
              <mrow>
                <mo>±</mo>
              </mrow>
              <mrow>
                <mn>3</mn>
                <mi>%</mi>
              </mrow>
            </math>
            , moderately up to
            <math>
              <mrow>
                <mo>±</mo>
              </mrow>
              <mrow>
                <mn>5</mn>
                <mi>%</mi>
              </mrow>
            </math>
            and gets much worse beyond that.
          </p>

        </section>
      </section>

      <section id="logged-predictor_example">
          <h3>Example</h3>

          <pre data-lang="R"><code>data(mtcars)
(mod2 <- lm(disp ~ log(drat), data = mtcars))</code></pre>

          <pre data-lang="R output"><code>Call:
lm(formula = drat ~ log(disp), data = mtcars)

Coefficients:
(Intercept)    log(disp)
     7.2301      -0.6875</code></pre>

          <p>
            Therefore a 25&percnt; increase in <code>disp</code> is associated
            with a
            <math>
              <mrow>
                <mo>−</mo>
              </mrow>
              <mrow>
                <mn>0.688</mn>
                <mrow>
                  <mspace width="0.1667em"></mspace>
                  <mi>log</mi>
                  <mo>⁡</mo>
                </mrow>
                <mo form="prefix" stretchy="false">(</mo>
                <mn>1.25</mn>
                <mo form="postfix" stretchy="false">)</mo>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mo form="prefix" stretchy="false">−</mo>
              </mrow>
              <mrow>
                <mn>0.153</mn>
              </mrow>
            </math>
            change in <code>drat</code>.
          </p>

          <p>
            To test this, we predict the difference in predicted outcome with some
            values of <code>disp</code>, and that value increased by25&percnt;.
          </p>

          <pre data-lang="R"><code>predict(mod2, newdata = data.frame(disp = 5)) - predict(mod2, newdata = data.frame(disp = 5*1.25))</code></pre>

          <pre data-lang="R output">        1
0.1534182</pre>

          <pre data-lang="R"><code>predict(mod2, newdata = data.frame(disp = 11)) - predict(mod2, newdata = data.frame(disp = 11*1.25))</code></pre>

          <pre data-lang="R output">        1
0.1534182</pre>

        </section>
        <section id="logged-predictor_viz">
          <h3>Visualization</h3>
          <p>
            We'll do a similar plot, except we'll let R handle the logging and
            exponentiating. We'll re-use <code>new_drat</code> created in the
            <a href="#logged-predictor_viz">earlier example</a>.
          </p>

        <pre data-lang="R"><code>yhat2 <- predict(mod2, newdata = data.frame(drat = new_drat))</code></pre>

        <pre data-lang="R"><code>plot(yhat2 ~ new_drat, type = "l")
with(mtcars, points(disp ~ drat))</code></pre>

        <img src="images/logx.svg" class="image_invert image_center"
        alt="Plot of disp versus drat from the model where drat (the predictor) is log-transformed">

      </section>
    </section>
    <section id="both-logged">
        <h2>Both Logged</h2>

        <p>
          A k&percnt; change in a predictor is associated with a
          <math>
            <msup>
              <mrow>
                <mo fence="true" form="prefix">(</mo>
                <mn>1</mn>
                <mo>+</mo>
                <mfrac>
                  <mi>k</mi>
                  <mn>100</mn>
                </mfrac>
                <mo fence="true" form="postfix">)</mo>
              </mrow>
              <mover>
                <mi>&beta;</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
            </msup>
          </math>
          multiplicative change in the outcome.
        </p>

        <p>
          Examples:
        </p>

        <ul>
          <li>
            If
            <math>
              <mover>
                <mi>&beta;</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
            </math>
            is 2, a 10&percnt; increase in X is associated with a
            <math>
              <mrow>
                <msup>
                  <mrow>
                    <mo fence="true" form="prefix">(</mo>
                    <mn>1</mn>
                    <mo>+</mo>
                    <mfrac>
                      <mn>10</mn>
                      <mn>100</mn>
                    </mfrac>
                    <mo fence="true" form="postfix">)</mo>
                  </mrow>
                  <mn>2</mn>
                </msup>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mn>1</mn>
                <mi>.</mi>
                <msup>
                  <mn>1</mn>
                  <mn>2</mn>
                </msup>
                <mo>≈</mo>
              </mrow>
              <mrow>
                <mn>1.21</mn>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mn>21</mn>
                <mi>%</mi>
              </mrow>
            </math>
            increase in Y.
          </li>
          <li>
            If
            <math>
              <mover>
                <mi>&beta;</mi>
                <mo stretchy="false" class="tml-capshift" style="math-style:normal;math-depth:0;">^</mo>
              </mover>
            </math>
            is -1.5, a 20&percnt; <strong>de</strong>crease in X is associated
            with a
            <math>
              <mrow>
                <msup>
                  <mrow>
                    <mo fence="true" form="prefix">(</mo>
                    <mn>1</mn>
                    <mo>+</mo>
                    <mfrac>
                      <mrow>
                        <mo form="prefix" stretchy="false" lspace="0em" rspace="0em">−</mo>
                        <mn>20</mn>
                      </mrow>
                      <mn>100</mn>
                    </mfrac>
                    <mo fence="true" form="postfix">)</mo>
                  </mrow>
                  <mrow>
                    <mo lspace="0em" rspace="0em">−</mo>
                    <mn>1.5</mn>
                  </mrow>
                </msup>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mi>.</mi>
                <msup>
                  <mn>8</mn>
                  <mrow>
                    <mo lspace="0em" rspace="0em">−</mo>
                    <mn>1.5</mn>
                  </mrow>
                </msup>
                <mo>≈</mo>
              </mrow>
              <mrow>
                <mn>1.40</mn>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mn>40</mn>
                <mi>%</mi>
              </mrow>
            </math>
            increase in Y.
          </li>
        </ul>
        <section id="both-logged_theory">
          <h3>Theory</h3>

          <p>
            To-do.
          </p>
        </section>
        <section id="both-logged_example">
          <h3>Example</h3>

          <pre data-lang="R"<code>data(mtcars)
(mod3 <- lm(log(disp) ~ log(drat), data = mtcars))</code></pre>

          <pre data-lang="R output">Call:
lm(formula = log(drat) ~ log(disp), data = mtcars)

Coefficients:
(Intercept)    log(disp)
     2.2763      -0.1905</pre>

          <p>
            Therefore a 25&percnt; increase in <code>disp</code> is associated
            with a
            <math>
              <mrow>
                <msup>
                  <mn>1.25</mn>
                  <mrow>
                    <mo lspace="0em" rspace="0em">−</mo>
                    <mi>.</mi>
                    <mn>1905</mn>
                  </mrow>
                </msup>
                <mo>=</mo>
              </mrow>
              <mrow>
                <mn>0.958</mn>
              </mrow>
            </math>
            multiplicative change in <code>drat</code>, corresponding to a
            4.2&percnt; decrease.
          </p>

          <p>
            To test this, we predict the difference in predicted outcome with
            some values of <code>disp</code>, and that value increased by
            25&percnt;.
          </p>

          <pre data-lang="R"<code>predict(mod3, newdata = data.frame(disp = 5)) - predict(mod3, newdata = data.frame(disp = 5*1.25))</code></pre>

          <pre data-lang="R output">         1
0.04251857</pre>

          <pre data-lang="R"<code>predict(mod3, newdata = data.frame(disp = 8)) - predict(mod3, newdata = data.frame(disp = 8*1.25))</code></pre>

          <pre data-lang="R output">         1
0.04251857</pre>
        </section>
        <section id="both-logged_viz">
          <h3>Visualization</h3>
          <p>
            Again, a similar plot, letting R handle the log in the predictor,
            and we'll manually exponentiate the outcome. Again, re-use
            <code>new_drat</code> created in the
            <a href="#logged-predictor_viz">earlier example</a>.
          </p>

        <pre data-lang="R"><code>yhat3 <- exp(predict(mod3, newdata = data.frame(drat = new_drat)))</code></pre>

        <pre data-lang="R"><code>plot(yhat3 ~ new_drat, type = "l")
with(mtcars, points(disp ~ drat))</code></pre>

        <img src="images/logboth.svg" class="image_invert image_center"
        alt="Plot of disp versus drat from the model where both predictor and outcome are log-transformed">

      </section>
    </section>
    <section id="viz-compared">
      <h2>Compare Visualizations</h2>

      <p>
        The three plots can look very similar, let's plot them simultaneously
        just to show that they are in fact three different curves.
      </p>

      <pre data-lang="R"><code>with(mtcars, plot(disp ~ drat))
lines(yhat1 ~ new_drat, col = "red")
lines(yhat2 ~ new_drat, col = "blue")
lines(yhat3 ~ new_drat, col = "green")</code></pre>

      <img src="images/log_all.svg" class="image_invert image_center"
        alt="Plot of disp versus drat with all three curves">

    <img src="https://errickson.goatcounter.com/count?p=logtransform">
</div>
	<nav class="section-nav no-print">
    <h2 id="header_nav">Log Transform Interpretation</h2>
		<ol>
			<li class="">
        <a href="#introduction">Introduction</a>
        <ol>
          <li class="">
            <a href="#introduction_change">Multiplicative vs Percentage
              change</a>
          </li>
        </ol>
      </li>
			<li class="">
        <a href="#logged-outcome">Logged Outcome</a>
			  <ol>
				  <li class="">
            <a href="#logged-outcome_theory">Theory</a>
          </li>
          <li class="">
            <a href="#logged-outcome_example">Example</a>
          </li>
          <li class="">
            <a href="#logged-outcome_viz">Visualization</a>
          </li>
        </ol>
      </li>
			<li class="">
        <a href="#logged-predictor">Logged Predictor</a>
			  <ol>
				  <li class="">
            <a href="#logged-predictor_theory">Theory</a>
            <ol>
              <li class="">
                <a href="#logged-predictor_approximation">Approximation</a>
              </li>
            </ol>
          </li>
          <li class="">
            <a href="#logged-predictor_example">Example</a>
          </li>
          <li class="">
            <a href="#logged-predictor_viz">Visualization</a>
          </li>
        </ol>
      </li>
			<li class="">
        <a href="#both-logged">Both Logged</a>
			  <ol>
				  <li class="">
            <a href="#both-logged_theory">Theory</a>
          </li>
          <li class="">
            <a href="#both-logged_example">Example</a>
          </li>
          <li class="">
            <a href="#both-logged_viz">Visualization</a>
          </li>
        </ol>
      </li>
      <li class="">
        <a href="#viz-compared">Visualizations Compared</a>
      </li>
    </ol>
