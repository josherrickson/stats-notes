<body class="statsnotes">
  <div class="content">
    <section id="introduction">
      <h1 id="header_content">Moderation and Mediation</h1>

      <p>
        Moderation and mediation are terms for measuring conditional effects. They
        are frequently discussed in the context of SEMs. However, both can be
        estimated in standard regression settings.
      </p>

      <p>
        (For mediation, while you <emph>can</emph> estimate via regression, I do
        not recommend it. See note below.)
      </p>

    </section>
    <section id="moderation">
      <h2>Moderation</h2>

      <p>
        LetX be a predictor, Y be a response and we are studying whether M
        moderates the relationship betweenX and Y. In other words, we want to
        know whether M modifies the strength (or direction) of the relationship
        between X and Y . For example, we could study the relationship between
        socio-economic status (X and frequency of breast exams (Y ). Among
        younger women, breast exams are rare because current recommendations are
        for women age 40 and over to get exams. However, in older women, there
        is a relationship because those with higher SES tend to be more
        concerned with health. Age (M) moderates the relationship between SES
        and frequency of breast exams.
      </p>

      <section id="moderation_in-regression">
        <h3>In Regression</h3>

        <p>
          Moderation is represented in a regression model with nothing more than
          an interaction term. There is a causal aspect to it, in that there
          needs to be the assumption that the predictor (X) has a causal
          relationship on the response (Y). If X is randomized, this assumption
          is satisfied. If the X is not randomized, this assumption must be
          based on theory and domain specific knowledge.
        </p>

        <p>
          Fit the interaction model,
        </p>

        <math display="block">
          <mrow>
            <mi>Y</mi>
            <mo>=</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>0</mn>
            </msub>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>1</mn>
            </msub>
            <mi>X</mi>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>2</mn>
            </msub>
            <mi>Z</mi>
            <mo>+</mo>
            <msub>
              <mi>&beta;</mi>
              <mn>3</mn>
            </msub>
            <mi>X</mi>
            <mi>Z</mi>
            <mo>+</mo>
            <mi>&epsilon;</mi>
            <mi>.</mi>
          </mrow>
        </math>

        <p>
          Therefore,
          <math>
            <msub>
              <mi>&beta;</mi>
              <mn>3</mn>
            </msub>
          </math>
          measures the moderation effect.
        </p>
      </section>
      <section id="moderation_sources">
        <h3>Sources</h3>

        <ul>
          <li><a href="http://davidakenny.net/cm/moderation.htm">http://davidakenny.net/cm/moderation.htm</a></li>
        </ul>
      </section>
    </section>
    <section id="mediation">
      <h2>Mediation</h2>

      <p>
        Again, let X be a predictor and Y be a response. We are studying whether
        M mediates the relationship between X and Y. Mediating variables are more
        complicated that moderators. A variable (M) is said to be a mediator if
        it partially or fully explains the relationship between the predictor
        (X) and the response (Y). The easiest way to explain this is visually.
      </p>

      <p>The unmediated and mediated model follow.</p>

      <img src="images/mediation.svg" class="image_center image_invert">

      <p>
        In the first model, the unmediated model, the total relationship between
        X and Y is captured by a. This model allows no mediation.
      </p>

      <p>
        In the second model, the total effect is split into a <strong>direct
        effect</strong> and an <strong>indirect effect</strong>. d is the direct
        effect and bc (the product of b and c) is the indirect effect from X to
        Y mediated through M.
      </p>

      <p>
        Depending on the significance (both statistical and practical) of d
        and bc, we would have:
      </p>

      <ul>
        <li>
          If d is non-zero but bc is zero, then we have no evidence of
          mediation.
        </li>
        <li>
          If both d and bc are non-zero then we have evidence
          of <strong>partial mediation</strong>. (There is evidence of both a
          direct effect and an indirect effect.)
        </li>
        <li>
          If d is zero, but bc is non-zero, then we have evidence
          of <strong>total mediation</strong>. (The only evidence of a
          relationship between X and Y is through the mediation of M.)
        </li>
        <li>
          Finally, if both d and bc are zero, there's no evidence of a
          relationship.
        </li>
      </ul>

      <p>
        We call (d + bc) the <strong>total effect</strong> between X and Y. In
        least squares regression (linear regression), we have that (a = d + bc).
        In all other models, that relationship may not hold.
      </p>

      <section id="mediation_in-regression">
        <h3>In Regression</h3>

        <p class="warning">
          While we <em>can</em> fit mediation via regression (called the
          Baron-Kenny approach), it is <strong>not</strong>
          recommended. Thinking of mediation as a series of linear regressions
          is great for conceptualizing mediation, but not for estimation. Use
          either the
          <a href="https://cran.r-project.org/package=lavaan">lavaan</a> or
          <a href="https://cran.r-project.org/package=mediation">mediation</a>
          packages in R or use <a href="https://www.stata.com/new-in-stata/causal-mediation-analysis/">causal
            mediation</a> in Stata.
        </p>

        <p>
          We can conceptalize the above model as three regression models, testing
          all paths above.
        </p>

        <ol start="1">
          <li>
            Test a to establish a relationship between predictor (X) and
            response (Y):
          </li>
        </ol>

        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>Y</mi>
            <mo>=</mo>
            <msubsup>
              <mi>&beta;</mi>
              <mn>0</mn>
              <mrow>
                <mo form="prefix" stretchy="false" lspace="0em" rspace="0em">(</mo>
                <mn>1</mn>
                <mo form="postfix" stretchy="false" lspace="0em" rspace="0em">)</mo>
              </mrow>
            </msubsup>
            <mo>+</mo>
            <mi>c</mi>
            <mi>X</mi>
            <mo>+</mo>
            <msub>
              <mi>&epsilon;</mi>
              <mn>1</mn>
            </msub>
          </mrow>
        </math>

        <ol start="2">
          <li>
            Test b to establish a correlation between predictor (X) and
            moderator (M):
          </li>
        </ol>

        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>Z</mi>
            <mo>=</mo>
            <msubsup>
              <mi>&beta;</mi>
              <mn>0</mn>
              <mrow>
                <mo form="prefix" stretchy="false" lspace="0em" rspace="0em">(</mo>
                <mn>2</mn>
                <mo form="postfix" stretchy="false" lspace="0em" rspace="0em">)</mo>
              </mrow>
            </msubsup>
            <mo>+</mo>
            <mi>a</mi>
            <mi>X</mi>
            <mo>+</mo>
            <msub>
              <mi>&epsilon;</mi>
              <mn>2</mn>
            </msub>
          </mrow>
        </math>

        <ol start="3">
          <li>
            Test c to establish that the mediator
            (M) affects the response
            (Y). Include both mediator
            (M) and predictor (X) to
            establish that the relationship exists when controlling
            for X.
          </li>
        </ol>

        <math display="block" class="tml-display" style="display:block math;">
          <mrow>
            <mi>Y</mi>
            <mo>=</mo>
            <msubsup>
              <mi>&beta;</mi>
              <mn>0</mn>
              <mrow>
                <mo form="prefix" stretchy="false" lspace="0em" rspace="0em">(</mo>
                <mn>3</mn>
                <mo form="postfix" stretchy="false" lspace="0em" rspace="0em">)</mo>
              </mrow>
            </msubsup>
            <mo>+</mo>
            <mi>d</mi>
            <mi>X</mi>
            <mo>+</mo>
            <mi>b</mi>
            <mi>M</mi>
            <mo>+</mo>
            <msub>
              <mi>&epsilon;</mi>
              <mn>3</mn>
            </msub>
          </mrow>
        </math>

      </section>
      <section id="mediation_sources">
        <h3>Sources</h3>
        <ul>
          <li>
            <a href="http://davidakenny.net/cm/mediate.htm">http://davidakenny.net/cm/mediate.htm</a>
          </li>
          <li>
            <a href="http://data.library.virginia.edu/introduction-to-mediation-analysis/">http://data.library.virginia.edu/introduction-to-mediation-analysis/</a>
          </li>
        </ul>
      </section>
    </section>
    <img src="https://errickson.goatcounter.com/count?p=moderationMediation">
  </div>
  <nav class="section-nav">
    <h2 id="header_nav">Moderation and Mediation</h2>
	  <ol>
		  <li class=""><a href="#introduction">Introduction</a></li>
		  <li class=""><a href="#moderation">Moderation</a>
        <ol>
          <li class=""><a href="#moderation_in-regression">In regression</a></li>
          <li class=""><a href="#moderation_sources">Sources</a></li>
        </ol>
      </li>
			<li class=""><a href="#mediation">Mediation</a>
        <ol>
          <li class=""><a href="#mediation_in-regression">In regression</a></li>
          <li class=""><a href="#mediation_sources">Sources</a></li>
        </ol>
      </li>
    </ol>
