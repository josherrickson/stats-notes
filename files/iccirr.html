<body class="statsnotes">
  <div class="content">
    <section id="introduction">
      <h1 id="header_content">Intra-Class Correlation and Inter-rater Reliability</h1>

      <p>
        A few notes on agreement between raters.
      </p>

    </section>
    <section id="kappa">
      <h2>Cohen's &kappa;</h2>

      <p>
        Cohen's &kappa; can be used for agreement between two
        raters on categorical data. The basic calculation is
      </p>

      <math display="block"
        <mrow>
          <mi>&kappa;</mi>
          <mo>=</mo>
          <mfrac>
            <mrow>
              <msub>
                <mi>p</mi>
                <mi>a</mi>
              </msub>
              <mo>−</mo>
              <msub>
                <mi>p</mi>
                <mi>e</mi>
              </msub>
            </mrow>
            <mrow>
              <mn>1</mn>
              <mo>−</mo>
              <msub>
                <mi>p</mi>
                <mi>e</mi>
              </msub>
            </mrow>
          </mfrac>
          <mspace width="6px"/>
          <mo separator="true">,</mo>
        </mrow>
      </math>
      <p>
        where <math><msub><mi>p</mi><mi>a</mi></msub></math> is the percentage
        observed agreement and <math><msub><mi>p</mi><mi>e</mi></msub></math> is
        the percentage expected agreement by chance. Therefore &kappa; is what
        percentage of the agreement over chance is observed.
      </p>

      <p>
        Fleiss' &kappa; is an extension to more than two raters and has a similar form.
      </p>

      <p>
        A major flaw in either &kappa; is that for ordinal data, any disagreement is
        treated equal. E.g. on a Likert scale, ratings of 4 and 5 are just as
        disagreeable as ratings of 1 and 5. Weighted &kappa; addresses this by
        including a weight matrix which can be used to provide levels of
        disagreement.
      </p>

      <section id="kappa_sources">
        <h3>Sources</h3>

        <ul>
          <li><a href="https://en.wikipedia.org/wiki/Cohen's_kappa">https://en.wikipedia.org/wiki/Cohen's_kappa</a></li>
          <li><a href="http://john-uebersax.com/stat/kappa.htm">http://john-uebersax.com/stat/kappa.htm</a></li>
          <li><a href="https://www.stata.com/manuals/rkappa.pdf">https://www.stata.com/manuals/rkappa.pdf</a></li>
        </ul>
      </section>
    </section>
    <section id="icc">
      <h2>Intra-class correlation</h2>

      <p>
        ICC is used for continuous measurements. It can be used in place of
        weighted &kappa; with ordinal variables of course. The basic
        calculation is
      </p>

      <math display="block">
        <mrow>
          <mtext>ICC</mtext>
          <mo>=</mo>
          <mfrac>
            <msubsup>
              <mi>&sigma;</mi>
              <mi>w</mi>
              <mn>2</mn>
            </msubsup>
            <mrow>
              <msubsup>
                <mi>&sigma;</mi>
                <mi>w</mi>
                <mn>2</mn>
              </msubsup>
              <mo>+</mo>
              <msubsup>
                <mi>&sigma;</mi>
                <mi>b</mi>
                <mn>2</mn>
              </msubsup>
            </mrow>
          </mfrac>
        </mrow>
        <mspace width="6px"/>
        <mo separator="true">,</mo>
      </math>

      <p>
        where <math><msubsup><mi>&sigma;</mi><mi>w</mi><mn>2</mn></msubsup></math> and
        <math><msubsup><mi>&sigma;</mi><mi>b</mi><mn>2</mn></msubsup></math> represent
        within- and between- rater variability respectively. Since the
        denominator is the total variance of all ratings regardless of rater,
        this fraction represents the percent of total variation accounted for by
        within-variation.
      </p>

      <p>
        The modern way to estimate the ICC is by a mixed model, extracting the
        &sigma;'s that are needed.
      </p>

      <section id="icc_r">
        <h3>ICC in R</h3>

        <p>
          Use the "Orthodont" data from <code>nlme</code> as our example. Look
          at <code>distance</code> measurements and look at correlation
          by <code>Subject</code>.
        </p>


        <pre><span class="keyword">library</span>(<span class="string">"nlme"</span>)
<span class="keyword">library</span>(<span class="string">"lme4"</span>)
<span class="keyword">data</span>(Orthondont)</pre>

        <section id="icc_nlme">
          <h4>With <code>nlme</code></h4>

          <p>
            Using the <code>nlme</code> package, we fit the model:
          </p>


          <pre>fm1 <span class="operator">&lt;-</span> <span class="keyword">lme</span>(distance <span class="operator">~</span> <span class="numeric">1</span>, <span class="keyword">random</span> <span class="operator">= ~</span> <span class="numeric">1</span> | Subject, <span class="keyword">data</span> <span class="operator">=</span> Orthodont)
<span class="keyword">summary</span>(fm1)</pre>

          <pre class="r_output"> Linear mixed-effects model fit by REML
  Data: Orthodont
       AIC      BIC    logLik
  521.3618 529.3803 -257.6809

Random effects:
 Formula: ~1 | Subject
        (Intercept) Residual
StdDev:    1.937002 2.220312

Fixed effects:  distance ~ 1
               Value Std.Error DF  t-value p-value
(Intercept) 24.02315 0.4296606 81 55.91192       0

Standardized Within-Group Residuals:
       Min         Q1        Med         Q3        Max
-3.2400448 -0.5277439 -0.1072888  0.4731815  2.7687301

Number of Observations: 108
Number of Groups: 27</pre>

          <p>
            The between-effect standard deviation is reported as
            the <code>Residual StdDev</code>. To obtain the ICC, we compute each
            &sigma;:
          </p>

          <pre>s2w <span class="operator"><-</span> <span class="keyword">getVarCov</span>(fm1)[[<span class="numeric">1</span>]]
s2b <span class="operator"><-</span> fm<span class="operator">1$</span>s^<span class="numeric">2</span>
<span class="keyword">c</span>(sigma2_w <span class="operator">=</span> s2w, sigma2_b <span class="operator">=</span> s2b, icc <span class="operator">=</span> s2w<span class="operator">/</span>(s2w <span class="operator">+</span> s2b))</pre>

          <pre class="r_output"> sigma2_w  sigma2_b       icc
3.7519762 4.9297832 0.4321677</pre>

        </section>
        <section id="icc_lme4">
          <h4>With <code>lme4</code></h4>

          <p>
            Using the <code>lme4</code> package, we fit the model:
          </p>


          <pre>fm2 <- <span class="keyword">lmer</span>(distance <span class="operator">~</span> (<span class="numeric">1</span> <span class="operator">|</span> Subject), <span class="keyword">data</span> <span class="operator">=</span> Orthodont)
<span class="keyword">summary</span>(fm2)</pre>

          <pre class="r_output"> Linear mixed model fit by REML ['lmerMod']
Formula: distance ~ (1 | Subject)
   Data: Orthodont

REML criterion at convergence: 515.4

Scaled residuals:
    Min      1Q  Median      3Q     Max
-3.2400 -0.5277 -0.1073  0.4732  2.7687

Random effects:
 Groups   Name        Variance Std.Dev.
 Subject  (Intercept) 3.752    1.937
 Residual             4.930    2.220
Number of obs: 108, groups:  Subject, 27

Fixed effects:
            Estimate Std. Error t value
(Intercept)  24.0231     0.4297   55.91</pre>

          <p>
            The Variance column of the Random Effects table gives the within-subject
            (Subject) and between-subject (Residual) variances.
          </p>


          <pre>s2w <span class="operator"><-</span> <span class="keyword">summary</span>(fm2)<span class="operator">$</span>varcor<span class="operator">$</span>Subject[1]</span>
s2b <span class="operator"><-</span> <span class="keyword">summary</span>(fm2)<span class="operator">$</span>sigma<span class="operator">^</span><span class="numeric">2</span>
<span class="keyword">c</span>(sigma2_w <span class="operator">=</span> s2w, sigma2_b <span class="operator">=</span> s2b, icc <span class="operator">=</span> s2w<span class="operator">/</span>(s2w <span class="operator">+</span> s2b))</pre>

          <pre class="r_output"> sigma2_w  sigma2_b       icc
3.7519736 4.9297839 0.4321675</pre>


          </section>
      </section>
      <section id="icc_sources">
        <h3>Sources</h3>

        <ul>
          <li><a href="https://en.wikipedia.org/wiki/Intraclass_correlation">https://en.wikipedia.org/wiki/Intraclass_correlation</a></li>
          <li><a href="http://stats.stackexchange.com/questions/14976/intraclass-correlation-coefficients-icc-with-multiple-variables">http://stats.stackexchange.com/questions/14976/intraclass-correlation-coefficients-icc-with-multiple-variables</a></li>
          <li><a href="<http://john-uebersax.com/stat/icc.htm">http://john-uebersax.com/stat/icc.htm</a></li>
        </ul>
      </section>
    </section>
    <img src="https://errickson.goatcounter.com/count?p=iccirr">
	</div>
	<nav class="section-nav">
    <h2 id="header_nav">Intra-Class Correlation and Inter-rater Reliability</h2>
	  <ol>
			<li class=""><a href="#introduction">Introduction</a></li>
			<li class=""><a href="#kappa">Cohens &kappa;</a>
        <ol>
          <li class=""><a href="#kappa_sources">Sources</a></li>
        </ol>
      </li>
			<li class=""><a href="#icc">ICC</a>
			  <ol>
				  <li class=""><a href="#icc_r">ICC in R</a>
            <ol>
              <li class=""><a href="#icc_nlme">ICC in nlme</a></li>
              <li class=""><a href="#icc_lme4">ICC in lme4</a></li>
            </ol>
          </li>
          <li class=""><a href="#icc_sources">Sources</a></li>
        </ol>
      </li>
    </ol>
