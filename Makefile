SRC_DIR = files
DEST_DIR = _output

HTML_FILES := $(wildcard $(SRC_DIR)/*.html)
HTML_DEST_FILES := $(patsubst $(SRC_DIR)/%.html, $(DEST_DIR)/%.html, $(HTML_FILES))

.PHONY: all
all: $(HTML_DEST_FILES) copy_files _output/index.html _output/responsesurfaceplot.html ## Build all documents to _output/

_output/index.html: index.html header.html ## @ignored
	mkdir -p _output
	cp header.html header-tmp.html
	sed -i '' -e 's_TITLE_JE Statisics Notes_' header-tmp.html
	cat header-tmp.html index.html > $@
	rm header-tmp.html

$(DEST_DIR)/%.html: $(SRC_DIR)/%.html header.html footer.html ## @ignored
	mkdir -p _output
	cp header.html header-tmp.html
	cp $< tmp.html
	$(eval TITLE := $(shell sed -n '/<h1[^>]*>\(.*\)<\/h1>/s//\1/p' $< | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$$//' | sed 's/<\/code>//g; s/<code>//g'))
	echo $(info $$TITLE is [${TITLE}])
	sed -i '' -e 's_TITLE_$(TITLE)_' header-tmp.html
	cat header-tmp.html tmp.html footer.html > $@
	rm header-tmp.html tmp.html

_output/responsesurfaceplot.html: files/responsesurfaceplot.Rmd ## @ignored
	R -e 'getwd(); rmarkdown::render("$<", output_file="../$@")'

.PHONY: open
open: ## Preview the built site
	@open _output/index.html

.PHONY: clean
clean: ## Remove all built sites
	rm -rf $(DEST_DIR)

.PHONY: copy_files
copy_files: ## @ignored
	cp *.js _output/
	cp stats-notes-style.css _output/
	rm -rf _output/images
	mkdir -p _output/images
	cp files/images/*.{svg,png} _output/images
	cp files/visualizeCollinearity-files/*.png _output/images

.PHONY: check-valid-public-site
check-valid-public-site: ## @ignored
	@if [ ! -d "../public-site" ]; then \
		echo "../public-site does not exist."; \
		exit 1; \
	fi
	@if [ ! -d "../public-site/.git" ]; then \
		echo "../public-site is not a Git repository."; \
		exit 1; \
	fi

.PHONY: upload
upload: check-valid-public-site ## Copy site to public-site
	@echo Syncing to ../public-site/public
	@mkdir -p ../public-site/public/stats-notes
	@rsync -rv --delete _output/ ../public-site/public/stats-notes
